import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//  添加upstream地址 https://www.dazhuanlan.com/2019/12/16/5df70051542ec/
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter layout demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter layout demo'),
        ),
        body: Center(
          // child: Text('Hello World'),
          // child: _buildImageColumn(),
          child: _buildGrid(),
        ),
      ),
    );
  }

  Widget _buildGrid() => GridView.extent(
        maxCrossAxisExtent: 150,
        padding: const EdgeInsets.all(4),
        children: _buildGridTileList(40),
      );

  Widget _buildImageColumn() => Container(
        decoration: BoxDecoration(
          color: Colors.black26,
        ),
        child: Column(
          children: [
            _buildImageRow(1),
            _buildImageRow(3),
          ],
        ),
      );

  // 制造一行 水平排列的图标
  Widget _buildImageRow(int imageIndex) => Row(
        children: [
          _buildDecoratedImage(imageIndex),
          _buildDecoratedImage(imageIndex + 1),
          // _buildVerticalImage(),
        ],
      );

  //本来想弄个竖直排列的图片 失败了：不使用Expanded 即可
  Widget _buildVerticalImage() => Column(
        children: [_buildDecoratedImage1(1)],
      );

  Widget _buildDecoratedImage1(int imageIndex) => Container(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.green),
            borderRadius: const BorderRadius.all(const Radius.circular(8)),
          ),
          margin: const EdgeInsets.all(4),
          child: Image.asset('ic_launcher.png'),
        ),
      );

  //制造一个图标
  Widget _buildDecoratedImage(int imageIndex) => Expanded(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.green),
            borderRadius: const BorderRadius.all(const Radius.circular(8)),
          ),
          margin: const EdgeInsets.all(4),
          child: Image.asset('ic_launcher.png'),
        ),
      );

  List<Container> _buildGridTileList(int count) => List.generate(
      count,
      (index) => Container(
            child: Image.asset('ic_launcher.png'),
          ));
}
