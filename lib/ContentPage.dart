import 'package:flutter/material.dart';

void main() {
  runApp(ContentPage(1));
}

class ContentPage extends StatefulWidget {
  ContentPage(this.counter);

  int counter = 0;

  @override
  _ContentPageState createState() => _ContentPageState();
}

class _ContentPageState extends State<ContentPage> {
  @override
  Widget build(BuildContext context) {
    // 使用Column
    return Container(
      alignment: Alignment.center,
      width: 300.0,
      height: 200.0,
      decoration: buildBox(),
      constraints: new BoxConstraints.expand(
          height: Theme.of(context).textTheme.display1.fontSize * 1.1 + 200.0),
      child: new Text(
        "容器显示...aa",
        textDirection: TextDirection.ltr,
      ),
    );
  }

  Decoration buildBox() {
    return new BoxDecoration(
        color: Colors.green,
        border: new Border.all(
          color: Colors.red,
          width: 3.0,
        ));
  }
}
